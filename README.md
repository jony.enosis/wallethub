## Environment Setup : ##
1. Install Java 14 if not already installed.
2. Verify Java 14 is installed properly. Open cmd and type java -version and then press enter. if the java version is shown that means java installation successful.
Troubleshoot: Set java path in the system environment variable. (if java version doesn't show in console) 


## Run desired automated test case : ##
1. Navigate to 'automation_project\Data' folder 
2. Open 'TestData.xlsx' file and add set value in Username and Password column for both assignment # 1 and 2
3. Go back to the 'automation_project' folder and double click on the 'exeution.bat' file.
4. Test execution reports can be found in 'automation_project\TestReports' folder with current date and time based on the test run.

## Note : ##
Below mentioned 2 points need account verification. Hence the below steps have not been automated.
1. If you are successful, you should see a confirmation screen saying you have reviewed the institution. You then have to go to your profile and confirm that the “review feed” got updated. 
2. Go to https://wallethub.com/profile/ and assert that you can see the review.

