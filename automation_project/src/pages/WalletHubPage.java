package pages;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class WalletHubPage extends basePage {

    //[Start]: URLs
    public static final String walletHubJoinUrl = "https://wallethub.com/join/light";
    public static final String walletInsuranceCompanyUrl = "http://wallethub.com/profile/test_insurance_company/";
    //]End]: URLs

    //[Start]: Elements FindBy
    @FindBy(id = "em-ipt")
    WebElement emailAddressInputField;
    @FindBy(id = "pw1-ipt")
    WebElement passwordInputField;
    @FindBy(id = "pw2-ipt")
    WebElement confirmPasswordInputField;
    @FindBy(xpath = "//span[text()='Join']")
    WebElement joinButton;
    @FindBy(id = "get-my-report")
    WebElement getMyFreeCreditScoreReportCheckbox;
    @FindBy(xpath = "//div[@class='dropdown second']")
    WebElement policyDropdown;
    @FindBy(xpath = "//textarea[@placeholder='Write your review...']")
    WebElement reviewInputField;
    @FindBy(xpath = "//div[text()='Submit']")
    WebElement reviewSubmitButton;
    @FindBy(xpath = "//h1[text()='Before we publish your review we need you to verify your email.']")
    WebElement reviewVerificationLocator;
    //[End]: Elements FindBy

    /**
     * Constructor to initialize page factory elements
     */
    public WalletHubPage() {
        AjaxElementLocatorFactory ajaxElementLocatorFactory = new AjaxElementLocatorFactory(getCurrentWebDriver(), defaultTimeOut);
        PageFactory.initElements(ajaxElementLocatorFactory, this);
    }

    /**
     * Join wallet hub as light user
     *
     * @param username User name to register as wallet hub user
     * @param password Password to register as wallet hub user
     */
    public void joinWalletHubAsLightUser(String username, String password) {
        testStepInfo("[Start]: " + getMethodName());
        try {
            emailAddressInputField.sendKeys(username);
            passwordInputField.sendKeys(password);
            confirmPasswordInputField.sendKeys(password);
            if(getMyFreeCreditScoreReportCheckbox.isSelected())
                getMyFreeCreditScoreReportCheckbox.click();
            joinButton.click();
            testStepPassed("Successfully inserted username: '" + username + "', password: '" + password + "' values in input field and clicked on the 'Join' button");
        } catch (WebDriverException e) {
            testStepFailed("Failed to perform join operation due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: " + getMethodName());
    }

    /**
     * Hover over the rating star
     *
     * @param starNumber Star number to hover
     */
    public void hoverOverTheRatingStar(int starNumber) {
        testStepInfo("[Start]: " + getMethodName());
        String hoverStarLocator = "Hover the rating star locator#xpath=//div[@class='rv review-action ng-enter-element']//div[@class='rating-box-wrapper']/*[" + starNumber + "]";
        try {
            moveToElement(getElementBy(elementState.present, hoverStarLocator, true));
            testStepPassed("Successfully hover over the start number : '" + starNumber + "'");
        } catch (WebDriverException e) {
            testStepFailed("Failed to perform hover operation in star number '" + starNumber + "' due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: " + getMethodName());
    }

    /**
     * is rating star lighten up
     *
     * @param starNumber star number to check hover effect
     * @return True/False based on the star lighten up
     */
    public boolean isRatingStarLightenUp(int starNumber) {
        testStepInfo("[Start]: " + getMethodName());
        boolean starLightUpStatus = false;
        String hoverStarLocator = "Rating star lighten up locator#xpath=//div[@class='rv review-action ng-enter-element']//div[@class='rating-box-wrapper']/*[" + starNumber + "]//*[@stroke='#4ae0e1']";
        try {
            starLightUpStatus = isElementExist(hoverStarLocator);
            testStepPassed("Successfully hover over the start number : '" + starNumber + "'");
        } catch (WebDriverException e) {
            testStepFailed("Failed to get status of rating star number '" + starNumber + "' due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: " + getMethodName());
        return starLightUpStatus;
    }

    /**
     * click on rating star number
     *
     * @param starNumber Star number to click
     */
    public void clickOnRatingStarNumber(int starNumber) {
        testStepInfo("[Start]: " + getMethodName());
        String hoverStarLocator = "click on rating star locator#xpath=//div[@class='rv review-action ng-enter-element']//div[@class='rating-box-wrapper']/*[" + starNumber + "]";
        try {
            clickElement(hoverStarLocator);
            testStepPassed("Successfully clicked on the '" + starNumber + "' rating star");
        } catch (WebDriverException e) {
            testStepFailed("Failed to click on the star number '" + starNumber + "' due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: " + getMethodName());
    }

    /**
     * Select policy from policy dropdown
     *
     * @param policyName Policy name to select
     */
    public void selectPolicyFromPolicyDropdown(String policyName){
        testStepInfo("[Start]: "+getMethodName());
        String dropDownItem = "Dropdown item#xpath=//div[@class='dropdown second opened']//li[@class='dropdown-item' and text()='" + policyName + "']";
        try {
            policyDropdown.click();
            clickElement(dropDownItem);
            testStepPassed("Successfully selected policy : '" + policyName + "'");
        } catch (Exception e) {
            testStepFailed("Failed to select policy : '" + policyName + "' due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: "+getMethodName());
    }

    /**
     * Submit a review
     *
     * @param policyName Policy name for review
     * @param reviewText Review text for submission
     */
    public void submitAReview(String policyName, String reviewText){
        testStepInfo("[Start]: "+getMethodName());
        try {
            selectPolicyFromPolicyDropdown(policyName);
            reviewInputField.sendKeys(reviewText);
            reviewSubmitButton.click();
            testStepPassed("Successfully performed review submit operation");
        } catch (Exception e) {
            testStepFailed("Failed to perform review submit operation due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: "+getMethodName());
    }

    /**
     * Is review submission successful
     *
     * @return True/False based on the review submission
     */
    public boolean isReviewSubmissionSuccessful(){
        testStepInfo("[Start]: "+getMethodName());
        boolean reviewVerificationStatus = false;
        try {
            reviewVerificationStatus = reviewVerificationLocator.isDisplayed();
            testStepInfo("Submitted review found : '" + reviewVerificationStatus + "'");
        } catch (Exception e) {
            testStepFailed("Failed to verify review submission due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: "+getMethodName());
        return reviewVerificationStatus;
    }
}
