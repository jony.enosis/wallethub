package pages;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class FacebookPage extends basePage {

    //[Start]: URLs
    public static final String facebookUrl = "https://www.facebook.com/";
    //]End]: URLs

    //[Start]: Elements FindBy
    @FindBy(id = "email")
    WebElement usernameInputField;
    @FindBy(id = "pass")
    WebElement passwordInputField;
    @FindBy(xpath = "//button[text()='Log In']")
    WebElement loginButton;
    @FindBy(xpath = "//a[@aria-label='Facebook']")
    WebElement homeButton;
    @FindBy(xpath = "//div[@aria-label='Create a post']//span[contains(text(), \"What's on your mind\")]")
    WebElement createPostArea;
    @FindBy(xpath = "//div[contains(@aria-label, \"What's on your mind\")]//div[@class='_1mf _1mj']")
    WebElement createPostTextBox;
    @FindBy(xpath = "//span[text()='Post']")
    WebElement postButton;
    //]End]: Elements FindBy

    /**
     * Constructor to initialize page factory elements
     */
    public FacebookPage() {
        AjaxElementLocatorFactory ajaxElementLocatorFactory = new AjaxElementLocatorFactory(getCurrentWebDriver(), defaultTimeOut);
        PageFactory.initElements(ajaxElementLocatorFactory, this);
    }

    /**
     * Login in to facebook with valid credentials
     *
     * @param username User name of the facebook user
     * @param password Password of the facebook user
     */
    public void loginInToFacebook(String username, String password) {
        testStepInfo("[Start]: " + getMethodName());
        try {
            usernameInputField.sendKeys(username);
            passwordInputField.sendKeys(password);
            loginButton.click();
            testStepPassed("Successfully inserted username: '" + username + "', password: '" + password + "' values in input field and clicked on the 'Log In' button");
        } catch (WebDriverException e) {
            testStepFailed("Failed to perform login operation due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: " + getMethodName());
    }

    /**
     * Create a post in Facebook
     *
     * @param statusMessage Status message that you want to post
     */
    public void createAPostInFacebook(String statusMessage) {
        testStepInfo("[Start]: " + getMethodName());
        try {
            homeButton.click();
            createPostArea.click();
            createPostTextBox.sendKeys(statusMessage);
            postButton.click();
            testStepPassed("Successfully inserted status message: '" + statusMessage + "' and clicked on the 'Post' button");
        } catch (Exception e) {
            testStepFailed("Failed to perform post status operation due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: " + getMethodName());
    }

    /**
     * Verify is status posted successfully
     *
     * @param statusMessage Status message that was posted
     * @return True/False based on the post message availability
     */
    public boolean isStatusPostedSuccessfully(String statusMessage) {
        testStepInfo("[Start]: " + getMethodName());
        String postMessageLocator = "Post message locator#xpath=//div[@role='feed']//div[@role='article']//div[text()='" + statusMessage + "']";
        boolean isPostPresent = false;
        try {
            if (getElementStatusBy(elementState.visible, postMessageLocator, false))
                isPostPresent = true;
            testStepInfo("Post visibility status is : '" + isPostPresent + "'");
        } catch (Exception e) {
            testStepFailed("Failed to get post status availability due to \n" + getStackTrackAsString(e));
        }
        testStepInfo("[End]: " + getMethodName());
        return isPostPresent;
    }
}
