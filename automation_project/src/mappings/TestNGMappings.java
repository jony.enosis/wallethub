package mappings;

import org.testng.Assert;
import org.testng.annotations.*;
import modules.Browser;
import testSuites.Facebook;
import testSuites.WalletHub;
import utilities.*;

import java.lang.reflect.Method;

import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;

@Listeners({ExtentITestListenerClassAdapter.class})
public class TestNGMappings {

    Browser browser = Browser.getInstance();
    Common common = new Common();

    /**
     * Setup to open browser and maximize
     *
     * @param browserName browserName
     */
    @BeforeClass
    @Parameters({"browserName"})
    public void preCondition(String browserName) {
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Opening and maximizing '" + browserName + "' browser");
        browser.openBrowser(browserName);
        browser.maximize();
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Opened and maximized '" + browserName + "' browser");
    }

    /**
     * Post condition to close browser and rename extent report
     */
    @AfterClass
    @Parameters({"browserName"})
    public void postCondition(String browserName) {
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Performing post condition activities");
        browser.quit();
        FilesUtil.renameFile(Common.outputDirectory + "/", "index.html", "Execution_Report_" + Common.reportGenerationTimeStamp + ".html");
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Performed post condition activities");
    }

    /**
     * Before specific test case execution
     */
    @BeforeMethod
    public void beforeSpecificTestCaseExecution() {
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Deleting cookies before test case execution");
        browser.deleteCookies();
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Deleted cookies before test case execution");
    }

    /**
     * After specific test case execution
     */
    @AfterMethod
    @Parameters({"loginURL"})
    public void afterSpecificTestCaseExecution() {
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Updating report for '" + Common.executingTestCaseName.toString() + "' test case");
        Common.executingTestCaseName.setLength(0);
        Common.extentReports.flush();
        Common.totalFailureInCurrentTestCase = 0;
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Updated report for '" + Common.executingTestCaseName.toString() + "' test case");
    }

    /**
     * Log current test case status
     */
    public void logCurrentTestCaseStatus() {
        if (Common.totalFailureInCurrentTestCase > 0) {
            Assert.fail();
        }
    }

    /**
     * Prepare test data for current test case
     *
     * @param currentTestCaseMethodName Current test case method name
     */
    public void prepareTestData(String currentTestCaseMethodName) {
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Preparing test data for '" + currentTestCaseMethodName + "' test case");
        Common.testDataList.clear();
        Common.currentTestCaseIterationCounter = 0;
        common.getTestCaseTestData(currentTestCaseMethodName);
        Common.executingTestCaseName.append(currentTestCaseMethodName);
        Common.extentTest = Common.extentReports.createTest(Common.executingTestCaseName.toString());
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Prepared test data for '" + currentTestCaseMethodName + "' test case");
    }

    /**
     * Assignment # 1 : Login into Facebook and post a status in Facebook with "Hello Work" message
     *
     * @param currentTestCaseName current test case name
     */
    @Test
    public void assignment_1_Facebook(Method currentTestCaseName) {
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Execution started for '" + currentTestCaseName.getName() + "' test case");
        prepareTestData(currentTestCaseName.getName());
        Facebook facebook = new Facebook();
        for (int i = 0; i < Common.testDataList.size(); i++) {
            LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Iteration started for  test data set **'" + i + "'** of '" + currentTestCaseName.getName() + "' test case");
            facebook.postStatusInFacebook();
            common.changeExecutionCounter();
            LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Iteration Completed for test data set **'" + i + "'** of '" + currentTestCaseName.getName() + "' test case");
        }
        logCurrentTestCaseStatus();
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Execution completed for '" + currentTestCaseName.getName() + "' test case");
    }

    /**
     * Assignment # 2 : WalletHub joining as light user, rating and review submission
     *
     * @param currentTestCaseName current test case name
     */
    @Test
    public void assignment_2_WalletHub(Method currentTestCaseName) {
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Execution started for '" + currentTestCaseName.getName() + "' test case");
        prepareTestData(currentTestCaseName.getName());
        WalletHub walletHub = new WalletHub();
        for (int i = 0; i < Common.testDataList.size(); i++) {
            LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[Start]: Iteration started for  test data set **'" + i + "'** of '" + currentTestCaseName.getName() + "' test case");
            walletHub.walletHubJoiningRatingReviewing();
            common.changeExecutionCounter();
            LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Iteration Completed for test data set **'" + i + "'** of '" + currentTestCaseName.getName() + "' test case");
        }
        logCurrentTestCaseStatus();
        LogWriter.writeToLogFile(LogWriter.logLevel.INFO, "[End]: Execution completed for '" + currentTestCaseName.getName() + "' test case");
    }
}

