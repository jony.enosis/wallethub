package testSuites;

import pages.WalletHubPage;
import pages.basePage;

public class WalletHub extends basePage {


    /**
     * Assignment # 2 : WalletHub joining as light user, rating and review submission
     */
    public void walletHubJoiningRatingReviewing() {
        WalletHubPage walletHubPage = new WalletHubPage();
        String  userName = retrieveTestData("Username"),
                password = retrieveTestData("Password"),
                starNumberToClick = retrieveTestData("Star Number"),
                policyNameToSelect = retrieveTestData("Policy Name"),
                reviewText = getRandomString(Integer.parseInt(retrieveTestData("Review Text Length")));

        navigateToURL(WalletHubPage.walletHubJoinUrl);
        walletHubPage.joinWalletHubAsLightUser(userName, password);
        navigateToURL(WalletHubPage.walletInsuranceCompanyUrl);
        for (int starNumber = 1; starNumber <= 3; starNumber++) {
            walletHubPage.hoverOverTheRatingStar(starNumber);
            assertEquals(walletHubPage.isRatingStarLightenUp(starNumber), true,
                    "Star number '" + starNumber + "' was not lighten up after hovering over it");
        }
        walletHubPage.clickOnRatingStarNumber(Integer.parseInt(starNumberToClick));
        walletHubPage.submitAReview(policyNameToSelect, reviewText);
        assertEquals(walletHubPage.isReviewSubmissionSuccessful(), true,
                "Review was not submitted successfully");
    }
}
