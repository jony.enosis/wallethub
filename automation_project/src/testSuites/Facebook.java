package testSuites;

import pages.FacebookPage;
import pages.basePage;

public class Facebook extends basePage {

    /**
     * Assignment # 1 : Login into Facebook and post a status in Facebook with "Hello Work" message
     */
    public void postStatusInFacebook() {
        FacebookPage facebookPage = new FacebookPage();
        String postMessage = retrieveTestData("Post Status Message");

        navigateToURL(FacebookPage.facebookUrl);
        facebookPage.loginInToFacebook(retrieveTestData("Username"), retrieveTestData("Password"));
        facebookPage.createAPostInFacebook(postMessage);
        assertEquals(facebookPage.isStatusPostedSuccessfully(postMessage), true, "'" + postMessage + "' message should be visible");
    }
}
